package com.puravida.mn

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Single

@Controller('/')
class BookController {

    BookRepository bookRepository

    BookController(BookRepository bookRepository){
        this.bookRepository = bookRepository
    }

    @Get('/')
    Single<List<Book>> index(){
        Single.create{ emitter ->
            emitter.onSuccess(bookRepository.findAll())
        }
    }

    @Get('/test')
    Single<Book> test(){
        Single.create{ emitter->
            Book b = new Book(title:"${new Date()}", pages:1000)
            emitter.onSuccess bookRepository.save(b)
        }
    }

}
