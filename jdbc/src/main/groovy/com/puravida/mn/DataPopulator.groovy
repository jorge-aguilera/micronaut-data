package com.puravida.mn

import io.micronaut.context.event.ApplicationEventListener
import io.micronaut.discovery.event.ServiceStartedEvent

import javax.inject.Singleton

@Singleton
class DataPopulator implements ApplicationEventListener<ServiceStartedEvent> {

    BookRepository bookRepository

    DataPopulator(BookRepository bookRepository){
        this.bookRepository = bookRepository
    }

    @Override
    void onApplicationEvent(ServiceStartedEvent event) {
        bookRepository.save(new Book(title:"The Stand", pages:1000))

        println bookRepository.count()
    }
}
