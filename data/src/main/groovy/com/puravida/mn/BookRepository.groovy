package com.puravida.mn

import io.micronaut.data.annotation.*
import io.micronaut.data.model.*
import io.micronaut.data.repository.CrudRepository
import io.reactivex.Single

@Repository
interface BookRepository extends CrudRepository<Book, Long> {

    Book find(String title)

}
